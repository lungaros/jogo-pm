/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import cr.cwi.crescer.jogoEstrategia.Game;
import cr.cwi.crescer.jogoEstrategia.GameObserver;
import cr.cwi.crescer.jogoEstrategia.Player;
import cr.cwi.crescer.jogoEstrategia.User;
import cr.cwi.crescer.jogoEstrategia.resources.Inventory;
import cr.cwi.crescer.jogoEstrategia.resources.InventoryItem;
import cr.cwi.crescer.jogoEstrategia.resources.Resource;
import cr.cwi.crescer.jogoEstrategia.resources.ResourceDependence;
import cr.cwi.crescer.jogoEstrategia.resources.ResourceType;
import cr.cwi.crescer.jogoEstrategia.tasks.Production;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.HashMap;

/**
 *
 * @author Lucas Alves
 */
@WebServlet(urlPatterns = {"/NewServlet"})
public class NewServlet extends HttpServlet implements GameObserver {

    //teste de id do jogador
    int idJogadores = 0;
    
    HashMap<Integer, Game> Lista_game = new HashMap();
    public Integer Proximo_id = 1;
  
    //criar player
    final Inventory inventory = new Inventory();
    //map das dependencias
    HashMap<Resource, ResourceDependence> dependences = new HashMap();
    //map das capacidade
    HashMap<Resource, Production> capacity = new HashMap();

    public NewServlet(){
        
        // definição inicial do jogo: init
        inventory.addInventoryItem(new InventoryItem(new Resource("Ouro", ResourceType.GOLD), 1000));
        inventory.addInventoryItem(new InventoryItem(new Resource("Ferro", ResourceType.IRON), 1000));
        inventory.addInventoryItem(new InventoryItem(new Resource("Madeira", ResourceType.WOOD), 1000));
        inventory.addInventoryItem(new InventoryItem(new Resource("Comida", ResourceType.FOOD), 1000));
        inventory.addInventoryItem(new InventoryItem(new Resource("Ferreiro", ResourceType.BLACKSMITH), 1));
        inventory.addInventoryItem(new InventoryItem(new Resource("Farmer", ResourceType.FARMER), 2));
//      inventory.clone();

        //inventario da arqueiro
        Resource arq = new Resource("Arqueiro", ResourceType.ARCHER);
        Resource mad = new Resource("Madeira", ResourceType.WOOD);
        Resource gol = new Resource("Ouro", ResourceType.GOLD);
        Resource fer = new Resource("Ferro", ResourceType.IRON);
        //Resource fod = new Resource("Comida", ResourceType.FOOD);
        HashMap<Resource, Integer> map = new HashMap();
        map.put(mad, 2);
        //map.put(fod, 15);
        map.put(fer, 3);
        map.put(gol, 5);
        ResourceDependence resDep = new ResourceDependence();
        resDep.setResource(arq);
        resDep.setDependence(map);
        dependences.put(arq, resDep);
        Production prod = new Production(arq, 1);
        capacity.put(arq, prod);
        
        //inventario da guerreiro
        Resource warrior = new Resource("Guerrreiro", ResourceType.KNIGHT);
        HashMap<Resource, Integer> map1 = new HashMap();
        map1.put(mad, 2);
        //map.put(fod, 15);
        map1.put(fer, 3);
        map1.put(gol, 5);
        ResourceDependence resDep1 = new ResourceDependence();
        resDep1.setResource(warrior);
        resDep1.setDependence(map1);
        dependences.put(warrior, resDep1);
        Production prod1 = new Production(warrior, 1);
        capacity.put(warrior, prod1);
        
        //inventario da Mineirador
        Resource miner = new Resource("Mineirador", ResourceType.MINER);
        HashMap<Resource, Integer> map2 = new HashMap();
        map2.put(mad, 2);
        //map.put(fod, 15);
        map2.put(fer, 3);
        map2.put(gol, 5);
        ResourceDependence resDep2 = new ResourceDependence();
        resDep2.setResource(miner);
        resDep2.setDependence(map2);
        dependences.put(miner, resDep2);
        Production prod2 = new Production(miner, 1);
        capacity.put(miner, prod2);
        

//        try {
//            game.addPlayer(new Player(user, inventory, dependences, capacity));
//
//        } catch (Exception ex) {
//
//        }
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

//      jogador.getInventarios().forEach((inv) -> {
//	 Resource resource = geraResource(inv.getItem());
//	 final InventoryItem inventoryItem = new InventoryItem(resource, inv.getQuantidade().intValue());
//	 inventory.addInventoryItem(inventoryItem);
//      });
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            //http://localhost:8080/NewServlet?action=createRoom
            String start = request.getParameter("start");//ESSA PALAVRA INICIA O IF
            String action = request.getParameter("action");
            String id = request.getParameter("gameId");
            String nome = request.getParameter("nome"); 
 
            if("criou".equals(start))
            {
                User user = new User(idJogadores++, nome);   
                
            if ("createRoom".equals(action)) {
                Game game = new Game(Proximo_id++, dependences, 10, this);
                Lista_game.put(game.getId(), game);
                            
                try {
                    Lista_game.get(Integer.parseInt(id)).addPlayer(new Player(user, inventory, dependences, capacity));

                } catch (Exception ex) {
                }
                
                out.println("{\"game\":[ {\"id\":"+game.getId()+"}]}");
            }
            else if ("entryRoom".equals(action)) {//ARRUMAR
                //http://localhost:8080/NewServlet?action=entryRoom&gameId=1                
                for (Integer key : Lista_game.keySet()) {
                    Game g = Lista_game.get(key);
                    out.println("{\"Lista_game\":[{ \"list\":"+g.getId()+"}]}");
                }
            }                  
            
            //if("Toni".equals(nome))//confrimar se a string entra
            //{
            //out.println("{ \"Resposta\":[ {\"res\": \"CERTO \"}]}");
            //}
            
            //testar id do jogador
            //out.println("{ \"Resposta\":[ {\"res\": "+idJogadores+"}]}");
            
            //if("createPlayer".equals(action))// action era igual a id, mas eu troquei, qualqur coisa é so trocar action por id, aqui nos criaremos o jogador
           // {    
              //out.println("{ \"Resposta\":[ {\"res\": "+nome+"}]}");
               // out.println("{ \"Resposta\":[ {\"res\": \"DEU\"}]}");
            //}
            
           // else if("gameId".equals(id))//criei este reserva, aqui que nos iremos atribuir o usuario a sala
           // {
                
           // }           
            
            }
            //if conectou na sala dai cria player pelo id da sala
            //out.println("{ \"Inventory\":[ {\"item\":\"arq\"}]}");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    @Override
    public void onTurnEnd(Game game) {
      //tabela do inventario/
      //tabela do mapa
    }

    @Override
    public void onGameStart(Game game) {
        //enviar mapa
        //seta player no mapa
        //iniciar tempo
        
    }

    @Override
    public void onGameEnd(Game game) {
           //mostrar vencedor
    }

}
