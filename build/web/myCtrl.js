/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



app.controller("myCtrl", function ($scope, $http, $timeout) {

    $scope.conteiner = [];
    $scope.valorMadeira = 0;
    $scope.valorFerro = 0;
    $scope.valorOuro = 0;
    $scope.valorComida = 0;
    $scope.mostrarVar = 0;
    $scope.criar_jogador = true;
    $scope.menu = false;
    $scope.start = false;
    $scope.mapa = false;
    $scope.nome = "JogadorX";
    //$scope.lista_sala=true;

    $http.get("http://localhost:8080/jogo-pm/NewServlet")
            .then(function (response) {
                $scope.inventory = response.data.Inventory;
            }, function (response) {
                $scope.mensagem = "Erro ao executar chamada remota. "
                        + "Motivo: " + response.status + " - "
                        + response.statusText;
            });
            
    $scope.matrix = ["ferro", "ferro", "chao", "chao", "chao", "ferro", "chao",
        "chao", "madeira", "chao", "chao", "chao", "chao", "comida",
        "comida", "chao", "chao", "chao", "chao", "madeira", "chao",
        "jogador2", "chao", "chao", "chao", "chao", "chao", "jogador1",
        "comida", "madeira", "chao", "chao", "ferro", "chao", "comida",
        "chao", "chao", "chao", "chao", "madeira", "chao", "chao",
        "ouro", "chao", "chao", "chao", "chao", "ouro", "chao", ];

    $scope.GuardarId = function (x) {

        if ($scope.conteiner.length <= 48)
        {
            $scope.conteiner.push(x);
        }
    }
    
    
    
    $scope.coletarRecurso = function (x, index) {

        //teste de index
        //$scope.mostrarVar = index;
        var tempo;

        if (x == "madeira")
        {
            
            $scope.valorMadeira += 1;
            $scope.matrix[index] = "chao";

            tempo = Math.floor(Math.random() * 20000);            
            
            $timeout(function () {
            $scope.matrix[index] = "madeira";
            }, tempo);
            
        } else if (x == "ferro")
        {
            $scope.valorFerro += 1;
            $scope.matrix[index] = "chao";
            
            tempo = Math.floor(Math.random() * 10000);
            
            $timeout(function () {
            $scope.matrix[index] = "ferro";
            }, 2000);
        } else if (x == "ouro")
        {
            $scope.valorOuro += 1;
            $scope.matrix[index] = "chao";
            
            tempo = Math.floor(Math.random() * 10000);
            
            $timeout(function () {
            $scope.matrix[index] = "ouro";
            }, 2000);
        } else if (x == "comida")
        {
            $scope.valorComida += 1;
            $scope.matrix[index] = "chao";
            
            tempo = Math.floor(Math.random() * 10000);
            
            $timeout(function () {
            $scope.matrix[index] = "comida";
            }, 2000);
        }
         
    }

    $scope.criarSala = function() { 
        $http.post("http://localhost:8080/jogo-pm/NewServlet?start=criou&action=createRoom")//realiza a chamada enviando o conteudo de action
            .then(function (response) {
                $scope.criouSala = response.data.game; // criouSala é a variavel usada para manipular os dados do conteudo
                $scope.criar_jogador=false;
                $scope.menu=false;
                $scope.start=true;
                $scope.mapa=false;
                //$scope.lista_sala=false;
            }, function (response) {
                $scope.mensagem = "Erro ao executar chamada remota. "
                        + "Motivo: " + response.status + " - "
                        + response.statusText;
            });
    }
    
    $scope.entrar_Sala = function() {
        
         $http.post("http://localhost:8080/jogo-pm/NewServlet?start=criou&action=entryRoom")//PARA UMA CHAMADA FUNCIONAR, DEVE MANDAR O START=CRIOU PRIEMIRO, caso contrario, os ifs dentro do start nao rião funcionar
            .then(function (response) {
                $scope.entrou = response.data.Lista_game;
                $scope.criar_jogador=false;
                $scope.menu=false;
                $scope.start=false;
                $scope.mapa=false;
                $scope.lista_sala=true;
            }, function (response) {
                $scope.mensagem = "Erro ao executar chamada remota. "
                        + "Motivo: " + response.status + " - "
                        + response.statusText;
            });
            
    }
    
    $scope.criar_Player = function() {
        
         $http.post("http://localhost:8080/jogo-pm/NewServlet?start=criou&nome="+$scope.nome+"")//nome entrou, 
            .then(function (response) {
                $scope.criou = response.data.Resposta;
                $scope.criar_jogador = false;
                $scope.menu = true;
                $scope.start = false;
                $scope.mapa = false;
                //$scope.lista_sala=true;
            }, function (response) {
                $scope.mesage = "Erro ao executar chamada remota. "
                        + "Motivo: " + response.status + " - "
                        + response.statusText;
            });
            
        
    }
    
    $scope.comecar = function() {
        
         $http.post("http://localhost:8080/jogo-pm/NewServlet?action=start")
            .then(function (response) {
                $scope.criou = response.data;
                $scope.criar_jogador=false;
                $scope.menu=false;
                $scope.start=false;
                $scope.mapa=true;
                $scope.lista_sala=false;
            }, function (response) {
                $scope.mesage = "Erro ao executar chamada remota. "
                        + "Motivo: " + response.status + " - "
                        + response.statusText;
            });
            
    }
});    